import { configure } from '@kadira/storybook';

function loadStories() {
  require('../src/components/masthead/Masthead.stories');
  require('../src/components/newsletterSubscription/NewsletterSubscription.stories');
  // require as many stories as you need.
}

configure(loadStories, module);
