'use strict';

import React from 'react';
import $ from 'jquery';

require('ta-semantic-ui/semantic/dist/components/reset.css');
require('ta-semantic-ui/semantic/dist/components/site.css');
require('ta-semantic-ui/semantic/dist/components/grid.css');
require('ta-semantic-ui/semantic/dist/components/container.css');
require('ta-semantic-ui/semantic/dist/components/icon.css');
require('ta-semantic-ui/semantic/dist/components/label.css');
require('ta-semantic-ui/semantic/dist/components/list.css');
require('./Masthead.scss');

let MastheadComponent = React.createClass({
  propTypes: {
    mediaName: React.PropTypes.string,
    shareUrl: React.PropTypes.string,
    twitterShareText: React.PropTypes.string,
    inverted: React.PropTypes.bool
  },

  getDefaultProps: function () {
    return {
      mediaName: 'localhost',
      inverted: false
    }
  },

  getInitialState: function () {
    return {
      facebookShares: 0
    }
  },

  componentDidMount: function () {
    if (this.props.shareUrl) {

      // Get Facebook Share Count
      this.serverRequest =
        $.get(`http://graph.facebook.com/?id=${this.props.shareUrl}`,
          (result) => {
            console.log(result);
            this.setState({
              facebookShares: result.shares || 0
            });
          });
    }
  },

  componentWillUnmount: function () {
    this.serverRequest.abort();
  },

  render: function () {
    return (
      <div className="ta-masthead masthead-component">
        <div className="ui container ">
          <div className="ui two column middle aligned grid">
            <div className="column ta-logo">
              <a href={`http://${this.props.mediaName}.ch`}><img
                src={`//interaktiv.tagesanzeiger.ch/static/logos/${this.props.mediaName}.svg`}
                alt=""
                className="logo ui middle aligned image"/></a>
            </div>

            {/*region Conditional */}
            {(() => {
              // Conditionals have to be constructed using immediately called
              // functions within JSX
              if (this.props.shareUrl) {
                return (
                  <div className="right aligned column">
                    <div className="ui horizontal link list share">
                      <a
                        href={`https://www.facebook.com/sharer/sharer.php?u=${this.props.shareUrl}`}
                        className="fb-share item">
                        <div className="count ui mini right pointing label"
                             data-count="0"
                             style={{marginRight: '0.5em'}}>{this.state.facebookShares}
                        </div>
                        <i className="facebook square middle aligned icon"/>
                      </a>
                      <a
                        href={encodeURI(`https://twitter.com/intent/tweet?url=${this.props.shareUrl}&via=${this.props.mediaName}${this.props.twitterShareText ? `&text=${this.props.twitterShareText}` : ''}`)}
                        className="twitter-share item">
                        <i className="twitter middle aligned icon"/>
                      </a>
                    </div>
                  </div>
                )
              }
            })()}
            {/*endregion*/}

          </div>
        </div>
      </div>
    )
  },

});

export default MastheadComponent;
