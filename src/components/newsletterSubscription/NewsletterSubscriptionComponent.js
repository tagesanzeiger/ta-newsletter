'use strict';

import React from 'react';
import SubscriptionForm from '../subscriptionForm/SubscriptionFormComponent';

require('ta-semantic-ui/semantic/dist/components/grid.css');
require('ta-semantic-ui/semantic/dist/components/container.css');
require('ta-semantic-ui/semantic/dist/components/segment.css');
require('ta-semantic-ui/semantic/dist/components/header.css');
require('ta-semantic-ui/semantic/dist/components/list.css');
require('ta-semantic-ui/semantic/dist/components/icon.css');
require('ta-semantic-ui/semantic/dist/components/divider.css');
require('ta-semantic-ui/semantic/dist/components/image.css');
require('./NewsletterSubscription.scss');

let iphoneMockup = require('./iphone-mockup.jpg');

let NewsletterSubscriptionComponent = (props) => (
  <div className="newslettersubscription-component ui vertical fitted segment">
    <div className="ui container">
      <div className="ui stackable grid">
        <div className="row">
          <div className="ui four wide column ta-image">
            <img src={iphoneMockup} alt="" className="ui large centered image"/>
          </div>
          <div className="ui twelve wide column text container">
            <div className="ui large header">«Der Morgen»-Newsletter jetzt
              abonnieren
            </div>
            <p>Die Tages-Anzeiger Redaktion fasst für Sie täglich um 7:30 Uhr
              die wichtigsten Themen zusammen.</p>
            <ul className="ui bulleted infographic list">
              <li className="item">Redaktionelle Auswahl</li>
              <li className="item">Direkt in Ihrem Mail-Eingang</li>
              <li className="item">Täglich – auch am Wochenende</li>
            </ul>

            <div className="ui infographic list">
              <a className="item" href="//tagesanzeiger.ch">
                <i className="blue caret right icon"/>
                Zur aktuellen Ausgabe
              </a>
            </div>

            <SubscriptionForm />

            <div className="ui fitted vertical segment">
              <p>Interessiert Sie auch das kulturelle Angebot für Tages-Anzeiger
                Abonnenten?</p>
              <div className="ui infographic list">
                <a className="item"
                   href="//tagesanzeiger.ch">
                  <i className="blue caret right icon"/>
                  Abonnieren Sie jetzt die Carte-Blanche e-Angebote.
                </a>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
);

NewsletterSubscriptionComponent.displayName = 'NewsletterSubscriptionComponent';

// Uncomment properties you need
// NewsletterSubscriptionComponent.propTypes = {};
// NewsletterSubscriptionComponent.defaultProps = {};

export default NewsletterSubscriptionComponent;
