'use strict';

import React from 'react';

require('ta-semantic-ui/semantic/dist/components/form.css');
require('ta-semantic-ui/semantic/dist/components/input.css');
require('ta-semantic-ui/semantic/dist/components/button.css');
require('ta-semantic-ui/semantic/dist/components/segment.css');
require('./SubscriptionForm.scss');

let SubscriptionFormComponent = (props) => (
  <div
    className="subscriptionform-component ui vertical very fitted primary segment">
    <form action="" className="ui form">
      <div className="fluid field ui action input top attached">
        <input type="text" placeholder="Ihre e-Mail-Adresse"/>
        <button className="ui primary disabled button">Abonnieren</button>
      </div>
      <div className="ui fluid infographic container">
        Mit dem Klick auf «Abonnieren» akzeptieren Sie
          unsere <a>AGB</a> und <a>Datenschutzrichtlinien.</a>
      </div>

    </form>
  </div>
);

SubscriptionFormComponent.displayName = 'SubscriptionFormComponent';

// Uncomment properties you need
// SubscriptionFormComponent.propTypes = {};
// SubscriptionFormComponent.defaultProps = {};

export default SubscriptionFormComponent;
