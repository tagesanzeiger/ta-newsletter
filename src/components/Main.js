require('ta-semantic-ui/semantic/dist/components/reset.css');
require('ta-semantic-ui/semantic/dist/components/site.css');
require('styles/App.css');

import React from 'react';
import MastheadCompontent from './masthead/MastheadComponent';
import NewsletterSubscriptionComponent from './newsletterSubscription/NewsletterSubscriptionComponent';

class AppComponent extends React.Component {
  render() {
    return (
      <div className="index">
        <MastheadCompontent mediaName="tagesanzeiger" shareUrl="http://newsletter.tagesanzeiger.ch" twitterShareText="Jetzt den Tages-Anzeiger-Newsletter abonnieren!" />
        <NewsletterSubscriptionComponent/>
      </div>
    );
  }
}

AppComponent.defaultProps = {};

export default AppComponent;
